/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodebate;

/**
 *
 * @author alunos
 */
public class Usuario {
    private String nome;
    private String cpf;
    private String telefone;
    private Debate debate;
    
  

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Debate getDebate() {
        return debate;
    }

    public void setDebate(Debate debate) {
        this.debate = debate;
    }
    
    
   
    
}
