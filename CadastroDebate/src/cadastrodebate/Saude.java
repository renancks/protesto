/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodebate;

/**
 *
 * @author alunos
 */
public class Saude extends Debate{
    private String hospital;
    
    public Saude(String titulo, String descricao, String hospital){
    
        super(titulo, descricao);
        this.hospital = hospital;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
    
}
