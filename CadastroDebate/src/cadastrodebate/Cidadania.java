/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastrodebate;

/**
 *
 * @author alunos
 */
public class Cidadania extends Debate{
    private String cidade;
    
    public Cidadania(String titulo, String descricao, String cidade){
    
        super(titulo, descricao);
        this.cidade = cidade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    
}
