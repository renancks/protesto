package model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author alunos
 */
public class Educacao extends Debate{
   
    private String ensino;
    
    Educacao(String titulo, String descricao, String ensino){
        
        super(titulo, descricao);
        this.ensino=ensino;
    }
    
    public String getEnsino() {
        return ensino;
    }

    public void setEnsino(String ensino) {
        this.ensino = ensino;
    }
   
    
    
    
}
