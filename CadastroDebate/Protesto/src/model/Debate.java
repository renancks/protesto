package model;

public class Debate {
	private String titulo;
    private String descricao;
    private String situacao;

    public Debate(String titulo, String descricao) {
        this.titulo = titulo;
        this.descricao = descricao;
    }

 
   

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    

}
