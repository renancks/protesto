package model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alunos
 */
public class Transporte extends Debate {
    
    private String empresa;
    
    public Transporte(String titulo, String descricao, String empresa){
    
        super(titulo, descricao);
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
    
}
